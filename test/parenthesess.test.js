const Parenthesess = require('../parenthesess')

describe('Test function parenthesess', () => {
  describe('Test function fail case', () => {
    describe('Case input is not a number', () => {
      test('Case input is a string', () => {
        expect(Parenthesess.generate('saya')).toBe('please use integer as an input')
      })
      test('Case input is array', () => {
        expect(Parenthesess.generate([1, 2])).toBe('please use integer as an input')
      })
      test('case input is an object', () => {
        expect(Parenthesess.generate({value: 1})).toBe('please use integer as an input')
      })
    })
    describe('Case doesn\'t have input', () => {
      test('case input blank or undefined', () => {
        expect(Parenthesess.generate()).toBe('please use integer as an input')
      })
    })
    describe('Case input not greater than 0', () => {
      test('case input 0', () => {
        expect(Parenthesess.generate(0)).toBe('please use positive integer as an input')
      })
    })
    describe('Case input is a float number', () => {
      test('case input float', () => {
        expect(Parenthesess.generate(0.5)).toBe('please use positive integer as an input')
      })
    })
  })
  describe('When input is the correct', () => {
    describe('Case input is integer and greater than 0', () => {
      test('case input is 3', () => {
        expect(Parenthesess.generate(3)).toStrictEqual([ '((()))', '()()()', '(())()', '()(())', '(()())' ])
      })
    })
  })
})