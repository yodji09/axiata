const PartOfSet = require('../PartOfSet')

describe('Test function parenthesess', () => {
  describe('Test function fail case', () => {
    describe('Case input is not a array', () => {
      test('Case input is a string', () => {
        expect(PartOfSet.generate('saya')).toBe('please input array type of data')
      })
      test('case input is an object', () => {
        expect(PartOfSet.generate({value: 1})).toBe('please input array type of data')
      })
    })
    describe('Case doesn\'t have input', () => {
      test('case input blank or undefined', () => {
        expect(PartOfSet.generate()).toBe('please input array type of data')
      })
      test('case input array length 0', () => {
        expect(PartOfSet.generate([])).toBe('there are no set on array')
      })
    })
  })
  describe('When input is the correct', () => {
    describe('Case input is integer and greater than 0', () => {
      test('case input is [1,3,5]', () => {
        expect(PartOfSet.generate([1, 3, 5, 6, 7])).toStrictEqual([
          [],             [ 1 ],
          [ 3 ],          [ 5 ],
          [ 6 ],          [ 7 ],
          [ 1, 3 ],       [ 1, 5 ],
          [ 1, 6 ],       [ 1, 7 ],
          [ 1, 3, 5 ],    [ 1, 3, 6 ],
          [ 1, 3, 7 ],    [ 1, 3, 5, 6 ],
          [ 1, 3, 5, 7 ], [ 1, 3, 5, 6, 7 ]
        ])
      })
    })
  })
})