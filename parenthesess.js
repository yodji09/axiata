class Parenthesess {
  static generate(number) {
    if(isNaN(number)) {
      return "please use integer as an input"
    }
    if(number <= 0 || number % 1 !== 0) {
      return "please use positive integer as an input"
    }
    let initialOpen = '('.repeat(number)
    let initialClose = ')'.repeat(number)
    let initialParenthesess = initialOpen + initialClose

    let tempResult = []
    let j = number
    tempResult.push(initialParenthesess)
    if(number === 1) {
      return tempResult
    }
    if(number%2 === 0) {
      let open = "(".repeat(number/2)
      let close = ")".repeat(number/2)
      let result = (open+close).repeat(2)
      tempResult.push(result)
    }
    while (j > 0) {
      let temp = []
      let parent = "()"
      if(number - j > 0) {
        let open = "(".repeat(j)
        let close = ")".repeat(j)
        let slot = parent.repeat(number-j)
        let open2 = "(".repeat(number-j)
        let close2 = ")".repeat(number-j)
        let result = open + close + slot
        let reverseResult = slot + open + close
        let result2 = open + close + open2 + close2
        let reverseResult2 = open2 + close2 + open + close
        if(tempResult.indexOf(result) == -1) {
          tempResult.push(result)
        }
        if(tempResult.indexOf(reverseResult) == -1) {
          tempResult.push(reverseResult)
        }
        if(tempResult.indexOf(result2) == -1) {
          tempResult.push(result2)
        }
        if(tempResult.indexOf(reverseResult2) == -1) {
          tempResult.push(reverseResult2)
        }
      }
      for (let i = j; i > 0; i--) {
        temp.push(parent)
      }
      if(temp.length < number) {
        let minus = number - temp.length
        let slot = parent.repeat(minus)
        for(let i = 0; i < temp.length; i++) {
          let holder = [...temp]
          holder[i] = holder[i].split('').join(slot)
          if(tempResult.indexOf(holder.join('')) == -1) {
            tempResult.push(holder.join(''))
          }
        }
      } else {
        if(tempResult.indexOf(temp.join(''))) {
          tempResult.push(temp.join(''))
        }
      }
      j--
    }

    return tempResult
  }
}

module.exports = Parenthesess