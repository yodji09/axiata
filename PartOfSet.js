class PartOfSet {
  static generate(array) {
    if(!array || !Array.isArray(array)) {
      return "please input array type of data"
    }
    if(array.length < 1) {
      return 'there are no set on array'
    }
    let result = []
    let index = []
    for (let i = 0; i < array.length; i++) {
      let temp = []
      if(i === 0) {
        result.push([])
      }
      index.push(i)
      for(let x = i+1; x < array.length; x++) {
        let temp2 = []
        if(index.length > 0) {
          index.forEach(ele => {
            temp2.push(array[ele])
          })
        }
        temp2.push(array[x])
        result.push(temp2)
      }
      result.push([array[i]])
    }
    
    result.sort((a, b) => {
      return a.length - b.length
    })
    return result
  }
}

module.exports = PartOfSet